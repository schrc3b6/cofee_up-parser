/* 1.5 main.c
 * Lara Kursawe (801813)
 * Pia Wolffram (572708)
 */

#include "ringbuf.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, const char * argv[]) {
	
	int length = argc - 1;
	for (int i = 1; i < argc; i++) {
		length += strlen(argv[i]);
	}
	
	ringbuf_t * testring = newRingbuf(length);
	
	for (int i = 1; i < argc; i++) {
		char * inputSentence = malloc(sizeof(char) * strlen(argv[i]));
		strcpy(inputSentence, argv[i]);
		insertInput(testring, inputSentence);
		insertInput(testring, " ");
	}
	
	readRingUntil(testring, '.');

	return 0;
	
}