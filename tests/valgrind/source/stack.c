#include "stack.h"
#include <stdlib.h>


stack_t* stack_create(size_t size){
    stack_t *stack = malloc(sizeof(stack_t));
    if (stack==NULL)
        return NULL;
    stack->capacity=size;
    stack->base = malloc(size);
    stack->top=stack->base;
    return stack;
}

void stack_delete(stack_t* stack){
    free(stack->base);
    free(stack);
}

size_t stack_size(stack_t* stack){
    return stack->top - stack->base;
}

int stack_push(stack_t* stack,char c){
    if(stack_size(stack)< stack->capacity){
        *stack->top=c;
        stack->top++;
        return 0;
    }
    return -1;
}

int stack_pop(stack_t* stack, char* dest){
    if(stack_size(stack)<1){
        return -1;
    }
    stack->top--;
    *dest=*stack->top;
    return 0; 
}
