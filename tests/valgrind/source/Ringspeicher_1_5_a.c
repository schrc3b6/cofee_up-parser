// Bibliothek für einen Ringspeicher (1.5a):

/* Mika Stozyk (Matrikel-Nr.: 802052)
 * Philomena Moek (Matrikel-Nr.: 804390)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Ringspeicher_1_5_a.h"


// Funktionsimplementationen:

/* create_ring(),
 *		reserviert Speicher für einen neuen leeren Ring 
 *		und setzt alle Pointer auf den Anfang des Rings.
 */
int create_ring(struct ringbuf_t * ringlist, size_t size) {
	ringlist->size = size;
	ringlist->start = malloc(size * sizeof(char) + 25);
	
	if (ringlist->start == NULL) {
		return -1;
	}
	ringlist->write_pos = ringlist->start;
	ringlist->read_pos = ringlist->start;
	return 0;
}	//create_ring


/* delete_ring(),
 *		gibt den in create_ring() reservierten Speicher wieder frei.
 */
int delete_ring(struct ringbuf_t * ringlist) {
	if (ringlist->start != NULL) {
		free(ringlist->start);
	}
	else {
		return -1;
	}
	return 0;
}	//delete_ring


/* insert,
 *		fügt einen char in den Ring ein.
 */
int insert(struct ringbuf_t * ringlist, char data) {
	if (ringlist->write_pos + 1 != ringlist->read_pos) {
		*(ringlist->write_pos) = data;
		ringlist->write_pos++;
	}
	else {
		return -1;
	}
	return 0;
}	//insert


/* read_size_characters(),
 *		liest eine Anzahl (size) Buchstaben aus dem Ring aus 
 *		und speichert diese als String unter der 
 *		als Parameter übergebenen Variable "read".
 */
int read_size_characters(struct ringbuf_t * ringlist, char * read, int size) {
	int i = 0;
	if (ringlist->start + ringlist->size <= ringlist->read_pos) {
		return -1;
	}
	while ((i<size) & (i < (int) ringlist->size)) {
		ringlist->read_pos++;
		*(read + i) = *(ringlist->read_pos - 1);
		i++;
	}
	*(read + i) = '\0';
	return 0;
}	//read_size_characters


/* read_till_d(),
 *		liest Buchstaben aus dem Ring aus, bis zu einem bestimmten char d.
 *		Wird d nicht gefunden stoppt die Funktion wenn sie 
 *		wieder am Anfang des Rings angekommen ist.
 *		Die ausgelesenen Buchstaben werden wie in read_size_characters() gespeichert.
 */
int read_till_d(struct ringbuf_t * ringlist, char * read, char d) {
	int i = 0;
	char merke;
	if (ringlist->start + ringlist->size <= ringlist->read_pos) {
		return -1;
	}
	while ((merke != d) & ((int) ringlist->size > i)) {
		ringlist->read_pos++;
		merke = *(ringlist->read_pos - 1);
		*(read + i) = merke;
		i++;
	}
	*(read + i) = '\0';
	return 0;
}	//read_till_d


/* delete_size_characters(),
 *		löscht eine Anzahl Buchstaben (size) vom Ring.
 */
int delete_size_characters(struct ringbuf_t * ringlist, int size) {
	for (int i=0; i<size; i++) {
		if (ringlist->start + ringlist->size <= ringlist->read_pos) {
			return -1;
		}
		*(ringlist->read_pos - 1) = ' ';
		ringlist->read_pos++;
	}
	return 0;
}	//delete_size_characters
