from cofee.gitlab_connector import GitlabConnector
from cofee.report import Report, ReportManager
from cofee.types import ErrorResult
import gitlab
import pandas as pd
import numpy as np
import os
import logging as log

def main():
    gitlab_token = os.getenv('GITLAB_TOKEN', None)
    if gitlab_token is None:
        log.error("Please set the GITLAB_TOKEN environment variable")
        exit(1)
    gitlab_url = os.getenv('GITLAB_URL', None)
    if gitlab_url is None:
        log.error("Please set the GITLAB_URL environment variable")
        exit(1)
    gl = gitlab.Gitlab(
        url=gitlab_url,
        private_token=gitlab_token,
    )
    rows = []
    error_kinds = set()

    gl = gitlab.Gitlab(
        url=gitlab_url,
        private_token=gitlab_token,
    )

    gbr2020 = gl.groups.get(10470)

    subgroups = gbr2020.subgroups.list(all=True)
    for g in subgroups:
        group = gl.groups.get(g.id)

        if not group.members.list():
            print(f"Skipping group '{group.name}' because it has no members.")
            continue

        projects = group.projects.list(all=True)
        for p in projects:
            project = gl.projects.get(p.id)

            gitlab_connector = GitlabConnector(private_token=gitlab_token, url=gitlab_url, project_id=project.id, pages_url="")
            data = gitlab_connector.get_previous_results_via_api()
            if data is None:
                print(f"No report found for {project.id}: {project.name}")
                continue

            report_manager = ReportManager()
            report_manager.import_from_str(data)
            report_manager.reports.sort(key=lambda x: int(x.pipeline_iid))

            for report_number, report in enumerate(report_manager.reports, 1):
                error_dict = report.get_errors_by_tool()

                flattened = {}
                for tool in error_dict:
                    for error in error_dict[tool]:
                        flattened[tool+":"+error.category] = error

                error_kinds.update(flattened.keys())

                rows.append([group.name, project.name, report_number, flattened])

    error_kinds = np.array(sorted(list(error_kinds)))

    for row in rows:
        error_dict = row.pop()

        tool_is_not_cmocka = np.vectorize(lambda error_kind: (error := error_dict.get(error_kind)) is not None and error.tool != "cmocka")
        non_cmocka_error_indices = tool_is_not_cmocka(error_kinds)

        kind_is_not_success = np.vectorize(lambda error_kind: (error := error_dict.get(error_kind)) is not None and error.kind != ErrorResult.SUCCESS)
        failed_test_indices = kind_is_not_success(error_kinds)

        error_values = np.zeros(len(error_kinds))
        error_values[failed_test_indices] = 1
        error_values[non_cmocka_error_indices] = 1

        row.extend(error_values.tolist())

    df = pd.DataFrame(data=rows, columns=["Gruppe", "Aufgabe", "Report Nummer"] + error_kinds.tolist())
    df.to_csv(path_or_buf="GBR-results-reports.csv", index=False)

if __name__ == "__main__":
    main()