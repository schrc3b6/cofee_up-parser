/*
 * Author: Max Schroetter
 * Purpose: Interface for a simple Ringbuffer
 */

#include <stdlib.h>

typedef struct ringbuf_t {
    char* data;
    size_t size,free;
    char* start;
    char* end;
} ringbuf_t;

/* allocate ringbuf_t with given size */
ringbuf_t* create_ringbuf(size_t size);

/* free ringbuf_t and referenced data */
void delete_ringbuf(ringbuf_t* buf);

/* append data of given size to ringbuf_t returns negative in case of errors*/
int append_ringbuf(ringbuf_t * buf, char* data, size_t size);

/* read size of bytes from the ringbuf_t and copys it to the target */
int read_ringbuf(ringbuf_t* buf, size_t size, char * target);
/* reads till delimiter d from the ringbuf_t and copys it to the target */
int read_delimited_ringbuf(ringbuf_t * buf, char d, char * target, size_t target_size);

/* remove n bytes from the ringbuf_t */
void remove_ringbuf(ringbuf_t* buf, size_t size);
