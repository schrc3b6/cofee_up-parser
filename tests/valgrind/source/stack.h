#include <stddef.h>

typedef struct stack{
    char* base;
    char* top;
    size_t capacity;
} stack_t;


stack_t* stack_create(size_t size);

void stack_delete(stack_t* stack);

size_t stack_size(stack_t* stack);

int stack_push(stack_t* stack,char c);

int stack_pop(stack_t* stack, char* dest);
