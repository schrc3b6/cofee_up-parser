#include <stdio.h>
#include "stack.h"

int main(){
    char c;
    stack_t *s = stack_create(20);
    printf("Stack size: %lu\n",stack_size(s));
    stack_push(s, 'a');
    stack_push(s, 'b');
    stack_push(s, 'c');
    printf("Stack size: %lu\n",stack_size(s));
    stack_push(s, c);
    printf("Poped: %c,",c);
    stack_pop(s, &c);
    printf("%c,",c);
    stack_pop(s, &c);
    printf("%c\n",c);
    printf("Stack size: %lu\n",stack_size(s));
    stack_delete(s);
}
