#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ringbuf.h"

#define RINGBUFSIZE 10

int main(int argc,char ** argv){

    if(argc < 2){
        fprintf(stderr,"Kein Argument übergeben!");
        exit(EXIT_FAILURE);
    }

    ringbuf_t *buf = create_ringbuf(RINGBUFSIZE);
    for( int i =1; i<argc;i++){
         if (append_ringbuf(buf,argv[i],strlen(argv[i])) <0) goto err;
         if (append_ringbuf(buf," ", 1)) goto err;
    }
    char * str= malloc(RINGBUFSIZE);
    if (read_delimited_ringbuf(buf,'.',str,RINGBUFSIZE) <0){
        fprintf(stderr,"Argumente enthalten keine ."); 
    }else{
    printf("%s",str);
    }
    return 0;

err:
        fprintf(stderr,"Not enough Space in Ringbuf");
        exit(EXIT_FAILURE);

}
