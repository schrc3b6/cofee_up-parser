// Programm, welches die Ringspeicherbibliothek nutzt (1.5b):

/* Mika Stozyk (Matrikel-Nr.: 802052)
 * Philomena Moek (Matrikel-Nr.: 804390)
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "Ringspeicher_1_5_a.h"

int main(int argc, char** argv) {
	
	if (argc < 2) {
		fprintf(stderr, "no arguments passed!\n");
		exit(EXIT_FAILURE);
	}
	
	// Erstellung einen neuen leeren Rings:
	struct ringbuf_t ring1;
	if (create_ring(&ring1, 200) < 0) {
		fprintf(stderr, "could not malloc!\n");
		exit(EXIT_FAILURE);
	}
	
	// Beschreiben des Rings mit den in argv übergebenen Sätzen:
	for (int i=1; i<argc; i++) {
		for (int j=0; j < (int) strlen(argv[i]); j++) {
			if (insert(&ring1, *(argv[i]+j)) < 0) {
				fprintf(stderr, "could not insert!\n");
				exit(EXIT_FAILURE);
			}
		}
		if (insert(&ring1, ' ') < 0) {
				fprintf(stderr, "could not insert!\n");
				exit(EXIT_FAILURE);
			}
	}
	
	
	// Auslesen und ausgeben des ersten Satzes aus dem Ring (bis zum ersten Punkt):
	char r_print;
	if (read_till_d(&ring1, &r_print, '.') < 0) {
		fprintf(stderr, "could not read!\n");
		exit(EXIT_FAILURE);
	}
	printf("%s", &r_print);
	
	
	/*
	read_size_characters(&ring1, &r_print, 5);
	printf("%s", &r_print);
	
	delete_size_characters(&ring1, 2);
	
	read_size_characters(&ring1, &r_print, 2);
	printf("%s", &r_print);
	*/
	
	printf("\n");
	return 0;
}	//main

