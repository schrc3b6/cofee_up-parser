/* 1.5 ringbuf.c
 * Lara Kursawe (801813)
 * Pia Wolffram (572708)
 */

#include "ringbuf.h"
#include <stdlib.h>
#include <stdio.h>

ringbuf_t * newRingbuf(int ringSize) {
	if (ringSize < 1) {
		printf("invalid size.");
		return NULL;
	}
	
	ringbuf_t * newRing = malloc(sizeof(ringbuf_t));
	
	newRing -> input = malloc(sizeof(char) * ringSize);
	newRing -> readIndex = 0;
	newRing -> writeIndex = 0;
	newRing -> ringSize = ringSize;
	
	return newRing;
}
	
void deleteRing(ringbuf_t * ring) {
	if(ring == NULL) {
		return;
	} else {
		free(ring -> input);
		ring -> input = NULL;
	}
}
	
void insertInput(ringbuf_t * ring, char * newInput) {
	if (newInput == NULL) {
		printf("invalid input.");
		return;
	}
	if(ring == NULL) {
		printf("invalid ring.");
	} else {
		char * i;
		for (i = newInput; *i != '\0'; i++) {
			if (ring -> writeIndex >= ring -> ringSize) {
				ring -> writeIndex = 0;
			}
			ring -> input[ring -> writeIndex] = *i;
			ring -> writeIndex = ring -> writeIndex + 1;
			//printf("%s\n", ring -> input);
		}	 
    }
}	
		
void readRingSize(ringbuf_t * ring, int size) {
	if (size < 1) {
		printf("invalid size.");
		return;
	} else {
		int i;
		for (i = 0; i < size; i++) {
			if (ring -> readIndex >= ring -> ringSize) {
				ring -> readIndex = 0;
			}
			if (ring -> input[i] == '\0') {
				break;
			}
			printf("%c", (char)(ring -> input[i]));
			ring -> readIndex = ring -> readIndex + 1;
		}
		printf("\n");
	}
}
	
	
void readRingUntil(ringbuf_t * ring, char d) {
	if(ring == NULL) {
		printf("invalid ring.");
	} else {
		//ring -> readIndex = 0;
		int i = 0;
		while (i < ring -> ringSize) {
			printf("%c", (char)(ring -> input[i]));
			if (ring -> input[i] == d) {
				return;
			} else {
				i++;
			}
		}
		printf("\n");
	}
}

void deleteCharacters(ringbuf_t * ring, int size) {
	if(ring == NULL) {
		printf("invalid ring.");
	} 
	if (size == 0) {
		return;
	} else {
		int i;
		for (i = 0; i < size; i++) {
			ring -> input[i] = NULL;
		}
	}
}
