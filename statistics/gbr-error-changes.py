from cofee.errorManager import ErrorManager
from cofee.parser import Parser
from cofee.types import Student, Project
import cofee.settings as settings
from cofee.types import HintAction,ErrorResult,ErrorType
from cofee.report import Report, ReportManager
from cofee.gitlab_connector import GitlabConnector
import pickle
import numpy as np

import matplotlib.pyplot as plt

project_id_dict={7647:0 ,7635: 1, 7825:2, 7826:3, 7933:4, 8035:5, 8034:6, 8136:7, 8137:8}

def increase_error_count_by_range(errdict, start, end):
    added = False
    start = int(start/10)
    end = int(end/10)
    for i in range(start, end+1):
        errdict[i] += 1
        added = True

def increase_error_count_by_project(errdict, project):
    if project in errdict.keys():
        errdict[project] += 1
    else:
        errdict[project] = 1

def get_num_of_reports(project):
    num_of_reports = 0
    for report in project.reportManager.reports:
        if report_has_cmocak_error(report):
            num_of_reports += 1
    return num_of_reports

# return true if the report contains any other errors than compilation errors
# this is an indicator that part or all of the code got compiled and was checked
def report_has_cmocak_error(report):
    for error in report.errors:
        if error.tool != "makefile":
            return True
    return False

def main():
    errdict=[0]*11
    projectdict={}
    error_count = 0
    project_set = set()
    student_set = set()
    with open("students.pickle", "rb") as f:
        students = pickle.load(f)
    for student in students:
        for project in student.projects:
            num_of_reports = get_num_of_reports(project)
            count = 0
            sorted_reports = sorted(project.reportManager.reports, key=lambda x: int(x.pipeline_iid))
            for report in sorted_reports:
                if not report_has_cmocak_error(report):
                    continue
                for error in report.errors:
                    if error.kind != ErrorResult.SUCCESS:
                        if "Using Variable" in error.category or "allocation" in error.category:
                            error_count+=1
                            project_set.add(project.id)
                            student_set.add(student.name)
                            if project.forkid == None:
                                if "Prozesstabelle" in project.name:
                                    increase_error_count_by_project(projectdict, 7825 )
                            else:
                                increase_error_count_by_project(projectdict, project.forkid)

                            increase_error_count_by_range(errdict, int(100*(count/num_of_reports)), int(100*((count+1)/num_of_reports)))
                count += 1
    print(error_count)
    for key in projectdict.keys():
        print(project_id_dict[key], projectdict[key])
    print(projectdict)
    print("number of projects: ", len(project_set), "number of students: ", len(student_set))
    print(project_set)
    print(student_set)
    index=range(0, 101, 10)
    plt.subplots_adjust(left=0.15, right=0.95, top=0.95, bottom=0.15)
    plt.rcParams["figure.figsize"] = (4,3)
    plt.rc('font', size=16)
    ax = plt.gca()
    ax.set_ylim([0, 30])
    ax.set_xticks([0,100],["first", "last"])
    ax.set_xticks([10,20,30,40,50,60,70,80,90],["","","","","in between","","","",""],minor=True)
    p,=ax.plot(index,errdict,label="Missing Errorhandling for malloc")
    ax.legend([p],[f"Missing Errorhandling\nfor Malloc & Calloc"],loc='upper right')
    plt.xlabel("Commits")
    plt.ylabel("Number of Errors")
    plt.savefig('./changes.pdf')


    error_count = 0
    for student in students:
        for project in student.projects:
            num_of_reports = len(project.reportManager.reports)
            count = 0
            report = project.reportManager.get_latest_report()
            if report is not None:
                for error in report.errors:
                    if error.kind != ErrorResult.SUCCESS:
                        if "Using Variable" in error.category or "allocation" in error.category:
                            error_count+=1
                            # print(student.name, project.name)

    # print(error_count)


if __name__ == "__main__":
    main()