from cofee.errorManager import ErrorManager
from cofee.parser import Parser
import cofee.settings as settings
from cofee.types import HintAction,ErrorResult,ErrorType
from cofee.report import Report, ReportManager
from cofee.gitlab_connector import GitlabConnector
import gitlab
import csv
import os
import logging as log

def get_error_dict(server_url, project_id, pages_url, gitlab_token: str):
    gitlab = GitlabConnector(private_token=gitlab_token, url=server_url, project_id=project_id, pages_url=pages_url )
    reports = ReportManager()
    data=gitlab.get_previous_results_via_api()
    if data is not None:
        reports.import_from_str(data)
    report= reports.get_latest_report()
    if report is None:
        return None
    return report.get_errors_by_tool()

def flatten_error_dict(error_dict):
    flattened = {}
    for tool in error_dict:
        for error in error_dict[tool]:
            flattened[tool+":"+error.category] = error
    return flattened

def get_all_kinds(results: dict):
    kinds = set()
    for student in results:
        for category in results[student]["errors"]:
            kinds.add(category)
    return kinds

def main():
    gitlab_token = os.getenv('GITLAB_TOKEN', None)
    if gitlab_token is None:
        log.error("Please set the GITLAB_TOKEN environment variable")
        exit(1)
    gitlab_url = os.getenv('GITLAB_URL', None)
    if gitlab_url is None:
        log.error("Please set the GITLAB_URL environment variable")
        exit(1)
    gl = gitlab.Gitlab(
        url=gitlab_url,
        private_token=gitlab_token,
    )
    gl = gitlab.Gitlab(
        url=gitlab_url,
        private_token=gitlab_token,
    )
    results = {}
    groups= [14197,14198]
    for group_id in groups:
        group = gl.groups.get(group_id)
        projects = group.projects.list(all=True)
        for group_project in projects:
            project = gl.projects.get(group_project.id)
            errs= get_error_dict(gitlab_url, project.id, "", gitlab_token=gitlab_token)
            if errs is None:
                print("No report found for project: " + project.name)
                continue
            results[project.name] = {"group": group_id, "errors": flatten_error_dict(errs)}
    kinds = get_all_kinds(results)
    with open('results-reports.csv', 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
        lkinds = list(kinds)
        lkinds.sort()
        csvwriter.writerow(["Name", "Gruppe"]+lkinds)
        for student in results:
            row = [student, results[student]["group"]]
            for kind in lkinds:
                if kind in results[student]["errors"]:
                    if results[student]["errors"][kind].tool == "cmocka":
                        if results[student]["errors"][kind].kind == ErrorResult.SUCCESS:
                            row.append(1)
                        else:
                            row.append(0)
                    else:
                        row.append(-1)
                else:
                    row.append(0)
            csvwriter.writerow(row)



if __name__ == "__main__":
    main()