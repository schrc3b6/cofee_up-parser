/*
 * Author: Max Schroetter
 * Purpose: This provides the implementation for a simple ring buffer.
 * It uses continious memory and standard opperations
 */
#include <stdlib.h>
#include "ringbuf.h"
#include <string.h>

/*
 * function: create_ringbuf
 * parameter: size_t size
 * returns: ringbuf_t*
 * description: create Ringbuffer with the given size
 */
ringbuf_t* create_ringbuf(size_t size)
{
    ringbuf_t* buf = malloc(sizeof(ringbuf_t));
    if (buf==NULL) {
        return NULL;
    }

    buf->data = malloc(size);
    if (buf->data==NULL) {
        return NULL;
    }
    buf->size=size;
    buf->free=size;
    buf->start=buf->data;
    buf->end=buf->data;
    return buf;
}

/*
 * function: delete_ringbuf
 * parameter: ringbuf* buf
 * returns: void
 * description: frees the memory of ringbuf
 */
void delete_ringbuf(ringbuf_t* buf)
{
    free(buf->data);
    free(buf);
}

/*
 * function: append_ringbuf
 * parameter: ringbuf* buf, char* data, size_t size
 * returns: int
 * description: Appends data with the given size to ringbuf.
 * For the student solution size can be ommited if they check internally with strlen
 */
int append_ringbuf(ringbuf_t* buf, char* data, size_t size)
{
    if (buf->free<size)
        return -1;
    // calculate remaining continious free space
    size_t rfspace = buf->data +buf->size - (buf->end);
    //check if we have to wrap data
    if(size > rfspace) {
        memcpy(buf->end,data,rfspace);
        memcpy(buf->data,data+rfspace,size-rfspace);
        buf->end=buf->data+(size-rfspace);
    } else {
        memcpy(buf->end,data,size);
        if(rfspace-size==0)
            buf->end=buf->data;
        else
            buf->end=buf->end+size;
    }
    buf->free-=size;
    return 0;
}

/*
 * function: delete_ringbuf
 * parameter: ringbuf* buf, size_t size, char *target
 * returns: void
 * description: reads size bytes from the ringbuf and writes the into the target string
 * Student solution:
 * The funktion must garantee that there is a \0 byte at the end of the string
 */
int read_ringbuf(ringbuf_t* buf, size_t size, char* target)
{
    // check if not to many bytes were requested
    if (buf->size - buf->free < size )
        return -1;
    size_t crspace = buf->data + buf->size - buf->start;
    // need to jump
    if (crspace < size ) {
        memcpy(target, buf->start,crspace);
        memcpy(target+crspace, buf->data,size-crspace);
    } else {
        memcpy(target,buf->data,size);
    }
    ((char*) target)[size]=0;

    return 0;
}

/*
 * function: delete_ringbuf
 * parameter: ringbuf* buf, char d, char* target, size_t target_size
 * returns: int
 * description: reads the bytes untill the delimiter d from the ringbuf.
 * Student solution:
 * The function must check if the target size is big enough and adds \0 to the end of the string
 */
int read_delimited_ringbuf(ringbuf_t* buf, char d, char* target,
                           size_t target_size)
{
    char* i= buf->start;
    size_t steps=0;
    int wrapped=0;
    while ( *i != d && steps < buf->size ) {
        if (i < buf->data + buf->size) {
            i++;
        } else {
            i=buf->data;
            wrapped=1;
        }
        steps++;
    }
    if(*i!=d)
        return -1;
    if(target_size <= steps) {
        return -1;
    }
    if (wrapped) {
        int cmsize=buf->data+buf->size - buf->start;
        memcpy(target,buf->start,cmsize);
        memcpy(target+cmsize,buf->data,i-buf->data);
    } else {
        memcpy(target,buf->start,i-buf->start);
    }
    ((char*) target)[steps]=0;
    return 0;
}

/*
 * function: delete_ringbuf
 * parameter: ringbuf* buf, char d, char* target, size_t target_size
 * returns: void
 * description: removes x bytes from the ringbuf. It does so by simply increasing the start pointer
 */
void remove_ringbuf(ringbuf_t* buf,size_t size)
{
    // check if we need to wrap
    if (buf->start + size >= buf->data +buf->size) {
        // new start= start     + to deleted character at the start
        buf->start = buf->data + (size - (buf->data + buf->size - buf->start));
    } else {
        buf->start = buf->start + size;
    }
}
