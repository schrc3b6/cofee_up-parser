// Bibliothek für einen Ringspeicher (1.5a):

/* Mika Stozyk (Matrikel-Nr.: 802052)
 * Philomena Moek (Matrikel-Nr.: 804390)
 */

/* Datentyp ringbuf_t,
 *		beinhaltet seine Größe und 3 Pointer auf char.
 *		 - den start-pointer, welcher immer auf den Anfang des Rings zeigt
 *		 - der write_pos-pointer zeigt auf das Ende des schon geschriebenen im Ring
 *		 - und der read_pos-pointer zeigt auf das Ende des schon gelesenen im Ring
 */
struct ringbuf_t {
	char * start;
	char * write_pos;
	char * read_pos;
	size_t size;
};

// Funktionsdefinitionen:
int create_ring(struct ringbuf_t * ringlist, size_t size);

int delete_ring(struct ringbuf_t * ringlist);

int insert(struct ringbuf_t * ringlist, char i);

int read_size_characters(struct ringbuf_t * ringlist, char * read, int size);

int read_till_d(struct ringbuf_t * ringlist, char * read, char d);

int delete_size_characters(struct ringbuf_t * ringlist, int size);
