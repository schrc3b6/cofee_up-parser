#define _REENTRANT		
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define NUM_THREADS 5

void *func (void *);	/* thread routine */

int main (){ 
  int i, status;

  int arg=0;

  pthread_t tid[NUM_THREADS];	/* array of thread IDs */

  for (i = 0; i < NUM_THREADS; i++){
    status = pthread_create (&tid[i], NULL, func,(void *) &arg);
    if (status != 0){
      printf("Error pthread_create(), error code %i\n", status);
      exit(EXIT_FAILURE); /* terminates the process with all threads */
    }
    arg++;
  }
  for (i = 0; i < NUM_THREADS; i++) {
    status = pthread_join (tid[i], NULL);
    if (status != 0){
      printf("Error pthread_join(), error code %i\n", status);
      exit(EXIT_FAILURE);
    }
  }

  return 0;
} /* main */

void *func (void *arg){
  printf ("this is: %d\n", *(int *) arg);
  sleep (1);
  return NULL;
}
