#include <stdio.h>
#include <stdlib.h>
#include "ringbuf_t.h"



struct ringbuf_t{
    char* first; // Pointer auf anfang
    char* end; // Pointer auf Ende
    char* read; // Pointer auf Lesbaren Buchstaben
    char* write; // Pointer auf freien Platz
    int ringSize; //Anzahl der Elemente im Ringbuffer
};

typedef struct ringbuf_t* ringp;
typedef struct ringbuf_t ringbuf;

ringp make(int size){
    ringp buf;
    buf->first = malloc(sizeof(buf) * size);
    buf->read = buf->first;
    buf->end = buf->first + size*sizeof(char);
    buf->write = buf->first;
    return buf;
}
void clear(ringp buf){
    free (buf->first); //Speicherplatz freigeben
    buf->read = NULL;   //Dangling Pointer verhindern
    buf->end = NULL;    //Dangling Pointer verhindern
    buf->write = NULL;  //Dangling Pointer verhindern
}

void add(char a, ringp buf){
    if (buf->write == buf->end){ //falls write am Ende: schreiben, dann zu anfang
        *buf->write = a;
        buf->write = buf->first;
    }
    // andere Fälle analog xD
    
}
int read(int size, ringp buf){}
int read_until(ringp buf){}
int delete_(int size, ringp buf){}

int main(int argc, char * args[]){
    ringp buffi = make(2*argc -1); //platz für Lehrzeichen
    for(int i = 0; i <= argc; i++){ // Gehe die Wörter durch
        int j = 0;
        while(args[i][j] != '\0'){
            add(args[i][j], buffi);
            j++;
        }
        add(" ",buffi);
    //to be continued
    }


}
