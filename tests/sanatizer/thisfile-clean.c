/* gcc -O -Wall beispiel_posix.c -lpthread -o beispiel_posix */

#define _REENTRANT		/* basic 2-lines for threads */
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define SLEEP_TIME 10
#define NUM_THREADS 5

void *sleeping (void *);	/* thread routine */

int main (){ 
  int i, status;

  /* Argument of thread function is pointer to integer */  
  /*
  int *arg = malloc(sizeof(int));
  if (arg == NULL) {
    perror("Could not allocate memory for arg");
    exit(EXIT_FAILURE);
  }
  */
  int arg[NUM_THREADS];
  for (int j = 0;j < NUM_THREADS;j++){
    arg[j]=j;
  }

  pthread_t tid[NUM_THREADS];	/* array of thread IDs */

  for (i = 0; i < NUM_THREADS; i++){
    status = pthread_create (&tid[i], NULL, sleeping,(void *) &arg[i]);
    if (status != 0){
      printf("Error pthread_create(), error code %i\n", status);
      exit(EXIT_FAILURE); /* terminates the process with all threads */
    }
  }
  for (i = 0; i < NUM_THREADS; i++) {
    status = pthread_join (tid[i], NULL);
    if (status != 0){
      printf("Error pthread_join(), error code %i\n", status);
      exit(EXIT_FAILURE);
    }
  }

  printf ("main() reporting that all %d threads have terminated\n", i);
  return 0;
} /* main */

void *sleeping (void *sleep_time){
  /* pthreadtypes.h : */
  /* typedef unsigned long int pthread_t; */
  printf ("this is: %d\n", *(int *) sleep_time);
  sleep (10);
  return NULL;
}
