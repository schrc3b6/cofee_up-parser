/* 1.5 ringbuf.h
 * Lara Kursawe (801813)
 * Pia Wolffram (572708)
 */

#ifndef ringbuf_h
#define ringbuf_h

#include <stdio.h>

typedef struct {
	char * input;
    int readIndex;
    int writeIndex;
    int ringSize;
} ringbuf_t;

ringbuf_t * newRingbuf(int ringSize);
void deleteRing(ringbuf_t * ring);
void insertInput(ringbuf_t * ring, char * newInput);
void readRingSize(ringbuf_t * ring, int size);
void readRingUntil(ringbuf_t * ring, char d);
void deleteCharacters(ringbuf_t * ring, int size);



#endif /* ringbuf_h */