from cofee.errorManager import ErrorManager
from cofee.parser import Parser
from cofee.types import Student, Project
import cofee.settings as settings
from cofee.types import HintAction,ErrorResult,ErrorType
from cofee.report import Report, ReportManager
from cofee.gitlab_connector import GitlabConnector
import pickle

def increase_error_count(errdict, key):
    if key in errdict:
        errdict[key]+=1
    else:
        errdict[key]=1

def main():
    errdict={}
    commits =0; projects=0; student_count=0
    with open("students.pickle", "rb") as f:
        students = pickle.load(f)
    for student in students:
        if len(student.projects) == 0:
            continue
        student_count+=1
        for project in student.projects:
            projects+=1
            for report in project.reportManager.reports:
                commits+=1
                for error in report.errors:
                    if error.kind != ErrorResult.SUCCESS:
                        increase_error_count(errdict,error.tool + ": " + error.category)
    print("Commits:",commits)
    print("Projects:",projects)
    print("Students:",student_count)
    # for key, value in errdict.items():
        # print(value,"\t",key)


if __name__ == "__main__":
    main()