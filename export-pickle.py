from cofee.errorManager import ErrorManager
from cofee.parser import Parser
from cofee.types import Student, Project
import cofee.settings as settings
from cofee.types import HintAction,ErrorResult,ErrorType
from cofee.report import Report, ReportManager
from cofee.gitlab_connector import GitlabConnector
import gitlab
import pickle
import os
import logging as log

def main():
    gitlab_token = os.getenv('GITLAB_TOKEN', None)
    if gitlab_token is None:
        log.error("Please set the GITLAB_TOKEN environment variable")
        exit(1)
    gitlab_url = os.getenv('GITLAB_URL', None)
    if gitlab_url is None:
        log.error("Please set the GITLAB_URL environment variable")
        exit(1)
    gl = gitlab.Gitlab(
        url=gitlab_url,
        private_token=gitlab_token,
    )
    students = []
    gbr2022=gl.groups.get(10470)
    student_subgroups=gbr2022.subgroups.list(all=True)
    for student_subgroup in student_subgroups:
        student_group = gl.groups.get(student_subgroup.id)
        student= Student(student_group.name)
        print(student_group.name)
        group_project_list=student_group.projects.list(all=True)
        for group_project in group_project_list:
            gitlab_project = gl.projects.get(group_project.id)
            project = Project(gitlab_project.name)
            project.id = gitlab_project.id
            print(gitlab_project.name)
            if hasattr(gitlab_project,'forked_from_project'):
                project.forkid = gitlab_project.forked_from_project["id"]
                print("forkid:", project.forkid)
            else:
                print("No fork")
            connector = GitlabConnector(private_token=gitlab_token, url=gitlab_url, project_id=gitlab_project.id, pages_url="" )
            project.reportManager = ReportManager()
            data=connector.get_previous_results_via_api()
            if data is not None:
                project.reportManager.import_from_str(data)
            student.add_project(project)
        students.append(student)
    student_dump = pickle.dumps(students)
    with open("students.pickle", "wb") as f:
        f.write(student_dump)





if __name__ == "__main__":
    main()